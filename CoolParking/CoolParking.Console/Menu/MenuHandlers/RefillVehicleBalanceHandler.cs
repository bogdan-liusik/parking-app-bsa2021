﻿namespace CoolParking.BL.Console.MenuHandlers
{
    using System;
    using CoolParking.BL.Services;
    using static MenuHelper;

    class RefillVehicleBalanceHandler : MenuHandler
    {
        public RefillVehicleBalanceHandler(ParkingService parkingService) : base(parkingService) { }

        public override void Handle()
        {
            try
            {
                new DisplayVehiclesInParkingHandler(ParkingService).Handle();
                Console.Write("Enter vehicle ID for which you want to change balance: ");
                string ID = Console.ReadLine();

                if (!ParkingService.Exists(ID))
                {
                    WriteErrorLine("There are no such ID.");
                    WriteEnterAnyKeyToContinue();
                    return;
                }

                Console.Write("Enter the amount you want to top up your balance: ");
                decimal sum;
                Decimal.TryParse(Console.ReadLine(), out sum);

                if(sum <= 0)
                {
                    WriteErrorLine("It must be greater thar 0.");
                    WriteEnterAnyKeyToContinue();
                    return;
                }

                ParkingService.TopUpVehicle(ID, sum);
                WriteSuccessLine("Successfully top upped.");
                WriteEnterAnyKeyToContinue();
            }
            catch (Exception exception)
            {
                WriteErrorLine(exception.Message);
            }
        }
    }
}
