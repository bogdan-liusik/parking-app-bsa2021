﻿namespace CoolParking.BL.Console
{
    using CoolParking.BL.Models;
    using CoolParking.BL.Services;
    
    class Program
    {
        static void Main(string[] args)
        {
            var withdrawTimer = new TimerService(Settings.WithdrawInterval);
            var logTimer = new TimerService(Settings.LogInterval);
            var logService = new LogService(Settings.LogPath);

            using (var parkingService = new ParkingService(withdrawTimer, logTimer, logService))
            {
                new Menu(parkingService).Start();
            }
        }
    }
}
