﻿namespace CoolParking.BL.Console.MenuHandlers
{
    using CoolParking.BL.Services;
    using static MenuHelper;

    class DisplayCurrentParkingBalanceHandler : MenuHandler
    {
        public DisplayCurrentParkingBalanceHandler(ParkingService parkingService) : base(parkingService) { }

        public override void Handle()
        {
            WriteSuccessLine($"Current parking balance: {ParkingService.GetBalance()} y.e");
            WriteEnterAnyKeyToContinue();
        }
    }
}
