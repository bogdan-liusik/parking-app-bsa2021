﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.

using CoolParking.BL.Interfaces;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        Timer _timer;

        public TimerService(double interval) 
        {
            Interval = interval;
            _timer = new Timer();
            _timer.Interval = Interval;
        }

        public double Interval { get; set; }

        public event ElapsedEventHandler Elapsed;

        public void Start()
        {
            _timer.Elapsed += Elapsed;
            _timer.Enabled = true;
        }

        public void Stop()
        {
            _timer.Enabled = false;
        }

        public void Dispose()
        {
            _timer.Dispose();
        }
    }
}
