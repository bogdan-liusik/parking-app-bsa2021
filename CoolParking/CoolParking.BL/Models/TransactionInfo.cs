﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.

using System;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        public TransactionInfo(string vehicleID, decimal amount) : this(DateTime.Now, vehicleID, amount) { }

        public TransactionInfo(DateTime transactionTime, string vehicleID, decimal amount)
        {
            TransactionTime = transactionTime;
            VehicleID = vehicleID;
            Sum = amount;
        }

        public DateTime TransactionTime { get; set; }

        public string VehicleID { get; set; }
        
        public decimal Sum { get; set; }

        public override string ToString()
        {
            return String.Format($"Time: {TransactionTime}, " +
                                 $"VehicleID: {VehicleID}, " +
                                 $"Sum {Sum}");
        }
    }
}
