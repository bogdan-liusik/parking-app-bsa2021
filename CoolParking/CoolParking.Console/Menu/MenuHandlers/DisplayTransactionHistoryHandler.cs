﻿namespace CoolParking.BL.Console.MenuHandlers
{
    using System;
    using CoolParking.BL.Services;
    using static MenuHelper;

    class DisplayTransactionHistoryHandler : MenuHandler
    {
        public DisplayTransactionHistoryHandler(ParkingService parkingService) : base(parkingService) { }

        public override void Handle()
        {
            try
            {
                WriteColorLine(ParkingService.ReadFromLog(), System.ConsoleColor.Yellow);
                WriteEnterAnyKeyToContinue();
            }
            catch (Exception)
            {
                WriteErrorLine("Now logs are empty (logging happens every minute).");
            }            
        }
    }
}
