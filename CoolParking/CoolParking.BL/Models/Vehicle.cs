﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.

using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        readonly string id;
        readonly VehicleType _vehicleType;
        static Random _random;
        const string idPattern = @"[A-Z]{2}-[0-9]{4}-[A-Z]{2}";

        static Vehicle()
        {
            _random = new Random();
        }

        public Vehicle(VehicleType vehicleType, decimal balance)
        {
            id = GenerateRandomRegistrationPlateNumber();
            _vehicleType = vehicleType;
            Balance = balance;
        }

        public Vehicle(string id, VehicleType vehicleType, decimal balance) : this(vehicleType, balance)
        {
            if (!Regex.IsMatch(id, idPattern))
            {
                throw new ArgumentException("It must match the template XX-YYYY-XX (X letter, Y number)");
            }
            if(balance < 0)
            {
                throw new ArgumentException("Balance must be positive!");
            }
            this.id = id;
        }

        public string Id => id;

        public VehicleType VehicleType => _vehicleType;

        public decimal Balance { get; internal set; }
        
        public static string GenerateRandomRegistrationPlateNumber()
        {
            return $"{RandomString(2)}-{_random.Next(0, 9999).ToString("D4")}-{RandomString(2)}";
        }

        private static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

            return new string(Enumerable.Range(1, length)
                .Select(_ => chars[_random.Next(chars.Length)]).ToArray());
        }
    }
}
