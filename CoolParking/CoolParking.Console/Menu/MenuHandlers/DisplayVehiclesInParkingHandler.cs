﻿namespace CoolParking.BL.Console.MenuHandlers
{
    using CoolParking.BL.Services;
    using static MenuHelper;

    class DisplayVehiclesInParkingHandler : MenuHandler
    {
        public DisplayVehiclesInParkingHandler(ParkingService parkingService) : base(parkingService) { }

        public override void Handle()
        {
            if(ParkingService.GetVehicles().Count == 0)
            {
                WriteSuccessLine("Now parking are empty.");
                WriteEnterAnyKeyToContinue();
                return;
            }
            WriteSuccessLine("Now in parking are such vehicles: ");
            foreach (var item in ParkingService.GetVehicles())
            {
                WriteColorLine($"ID: {item.Id} | Type: {item.VehicleType} with balance {item.Balance} y.o", System.ConsoleColor.Yellow);
            }
        }
    }
}
