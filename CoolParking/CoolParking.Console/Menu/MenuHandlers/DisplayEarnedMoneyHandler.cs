﻿namespace CoolParking.BL.Console.MenuHandlers
{
    using CoolParking.BL.Services;
    using static MenuHelper;

    class DisplayEarnedMoneyHandler : MenuHandler
    {
        public DisplayEarnedMoneyHandler(ParkingService parkingService) : base(parkingService) { }

        public override void Handle()
        {
            WriteSuccessLine($"Amount of earned money for the current period (before logging): {ParkingService.GetLastEarnedMoney()} y.e");
            WriteEnterAnyKeyToContinue();
        }
    }
}
