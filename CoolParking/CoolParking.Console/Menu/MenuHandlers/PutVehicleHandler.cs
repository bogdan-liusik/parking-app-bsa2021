﻿namespace CoolParking.BL.Console.MenuHandlers
{
    using System;
    using CoolParking.BL.Services;
    using CoolParking.BL.Models;
    using static MenuHelper;

    class PutVehicleHandler : MenuHandler
    {
        public PutVehicleHandler(ParkingService parkingService) : base(parkingService) { }

        public override void Handle()
        {
            try
            {
                if(ParkingService.GetFreePlaces() == 0)
                {
                    throw new InvalidOperationException("There are no places to add vehicle.");
                }

                int type;
                decimal balance;

                Console.WriteLine($"1. {VehicleType.PassengerCar}\n2. {VehicleType.Truck}\n3. {VehicleType.Bus}\n4. {VehicleType.Motorcycle}");
                Console.Write("Please, choose vehicle type (above): ");

                Int32.TryParse(Console.ReadLine(), out type);

                if (type > 4 || type < 1)
                {
                    Console.WriteLine("Type must be in range 1 - 4, try again.");
                    WriteEnterAnyKeyToContinue();
                    return;
                }

                VehicleType vehicleType = (VehicleType)type;

                Console.Write($"Enter balance (greater than {Settings.Prices[vehicleType]} y.e  for such type of the vehicle): ");
                Decimal.TryParse(Console.ReadLine(), out balance);

                if(balance < Settings.Prices[vehicleType])
                {
                    Console.WriteLine($"Balance must be greater than {Settings.Prices[vehicleType]} y.e");
                    WriteEnterAnyKeyToContinue();
                    return;
                }

                var vehicle = new Vehicle(vehicleType, balance);
                ParkingService.AddVehicle(vehicle);
                WriteSuccessLine($"Successfully added with ID {vehicle.Id}");
                WriteEnterAnyKeyToContinue();
            }
            catch (Exception exception)
            {
                WriteErrorLine(exception.Message);
            }
        }
    }
}
