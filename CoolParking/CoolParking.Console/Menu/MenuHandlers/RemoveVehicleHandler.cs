﻿namespace CoolParking.BL.Console.MenuHandlers
{
    using System;
    using CoolParking.BL.Services;
    using static MenuHelper;

    class RemoveVehicleHandler : MenuHandler
    {
        public RemoveVehicleHandler(ParkingService parkingService) : base(parkingService) { }

        public override void Handle()
        {
            try
            {
                new DisplayVehiclesInParkingHandler(ParkingService).Handle();
                Console.Write("To remove vehicle enter ID: ");
                string ID = Console.ReadLine();
                ParkingService.RemoveVehicle(ID);
                WriteSuccessLine("Successfully removed.");
                WriteEnterAnyKeyToContinue();
            }
            catch (Exception exception)
            {
                WriteErrorLine(exception.Message);
            }
        }
    }
}
