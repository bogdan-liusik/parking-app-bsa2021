﻿namespace CoolParking.BL.Console.MenuHandlers
{
    using CoolParking.BL.Services;
    using static MenuHelper;

    class DisplayFreeParkingSpacesHandler : MenuHandler
    {
        public DisplayFreeParkingSpacesHandler(ParkingService parkingService) : base(parkingService) { }

        public override void Handle()
        {
            WriteSuccessLine($"Now are available {ParkingService.GetFreePlaces()} free parking spaces.");
            WriteEnterAnyKeyToContinue();
        }
    }
}
