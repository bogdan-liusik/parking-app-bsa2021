﻿namespace CoolParking.BL.Console.MenuHandlers
{
    using CoolParking.BL.Services;

    abstract class MenuHandler
    {
        ParkingService _parkingService;

        public MenuHandler(ParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        public ParkingService ParkingService => _parkingService;

        public abstract void Handle();
    }
}
