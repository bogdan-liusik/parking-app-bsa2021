﻿namespace CoolParking.BL.Console.MenuHandlers
{
    using CoolParking.BL.Services;
    using static MenuHelper;

    class DisplayLastParkingTransactionsHandler : MenuHandler
    {
        public DisplayLastParkingTransactionsHandler(ParkingService parkingService) : base(parkingService) { }

        public override void Handle()
        {
            if(ParkingService.GetLastParkingTransactions().Length == 0)
            {
                WriteSuccessLine("No transactions have yet been made.");
                WriteEnterAnyKeyToContinue();
                return;
            }
            WriteSuccessLine("All parking transactions for the current period (before logging): ");
            foreach (var item in ParkingService.GetLastParkingTransactions())
            {  
                WriteColorLine(item.ToString(), System.ConsoleColor.Yellow);
            }
            WriteEnterAnyKeyToContinue();
        }
    }
}
