﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.

using System.IO;
using System.Reflection;
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static double LogInterval { get; private set; }

        public static double WithdrawInterval { get; private set; }

        public static Dictionary<VehicleType, decimal> Prices { get; private set; }

        public static int InitialParkingBalance { get; private set; }

        public static int ParkingCapacity { get; private set; }

        public static decimal FineCoefficient { get; private set; }

        public static string LogPath = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log";

        static Settings()
        {
            LogInterval = 60000; //miliseconds
            WithdrawInterval = 5000; //miliseconds
            InitialParkingBalance = 0;
            ParkingCapacity = 10;
            FineCoefficient = (decimal)2.5;
            Prices = new Dictionary<VehicleType, decimal>()
            {
                {VehicleType.PassengerCar, 2},
                {VehicleType.Truck, 5},
                {VehicleType.Bus, (decimal)3.5},
                {VehicleType.Motorcycle, 1},
            };
        }
    }
}
