﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.


using System;
using System.Linq;
using System.Collections.ObjectModel;
using CoolParking.BL.Models;
using CoolParking.BL.Interfaces;
using System.Collections.Generic;
using System.Timers;
using System.Text;
using System.IO;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        Parking _parking;
        List<TransactionInfo> transactions;
        ITimerService _withdrawTimer;
        ITimerService _logTimer;
        ILogService _logService;

        //public ParkingService() : this(new TimerService(Settings.WithdrawInterval), new TimerService(Settings.LogInterval), new LogService(Settings.LogPath)) { }

        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            _parking = Parking.GetInstance;
            transactions = new List<TransactionInfo>();

            _withdrawTimer = withdrawTimer;
            _logTimer = logTimer;
            _logService = logService;

            _withdrawTimer.Elapsed += DoTransaction;
            _logTimer.Elapsed += Log;

            _withdrawTimer.Start();
            _logTimer.Start();
        }

        public void AddVehicle(Vehicle vehicle)
        {
            if(_parking.Vehicles.Exists(v => v.Id == vehicle.Id))
            {
                throw new ArgumentException("There are already exists car with such ID, try again!");
            }

            if (_parking.Vehicles.Count >= GetCapacity())
            {
                throw new InvalidOperationException("There is no parking space available!");
            }

            if(vehicle.Balance < 0)
            {
                throw new ArgumentException("You must refill your balance!");
            }

            _parking.Vehicles.Add(vehicle);
        }

        public decimal GetBalance()
        {
            return _parking.Balance;
        }

        public int GetCapacity()
        {
            return Settings.ParkingCapacity;
        }

        public int GetFreePlaces()
        {
            return GetCapacity() - _parking.Vehicles.Count;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return transactions.ToArray();
        }

        public decimal GetLastEarnedMoney()
        {
            return GetLastParkingTransactions().Sum(t => t.Sum);
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return _parking.Vehicles.AsReadOnly();
        }

        public string ReadFromLog()
        {
            return _logService.Read();
        }

        public void RemoveVehicle(string vehicleId)
        {
            if(!_parking.Vehicles.Exists(v => v.Id == vehicleId))
            {
                throw new ArgumentException("No such vehicle in the parking!");
            }

            if (_parking.Vehicles.Find(v => v.Id == vehicleId).Balance < 0)
            {
                throw new InvalidOperationException("You must refill your balance!");
            }

            _parking.Vehicles.Remove(_parking.Vehicles.Find(v => v.Id == vehicleId));
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (sum < 0)
            {
                throw new ArgumentException("Sum can't be negative!");
            }

            if (!_parking.Vehicles.Exists(v => v.Id == vehicleId))
            {
                throw new ArgumentException("No such vehicle in the parking!");
            }

            _parking.Vehicles.Find(v => v.Id == vehicleId).Balance += sum;
        }

        public bool Exists(string vehicleID)
        {
            return _parking.Vehicles.Exists(v => v.Id == vehicleID);
        }

        private void DoTransaction(object sender, ElapsedEventArgs e)
        {
            foreach (var vehicle in _parking.Vehicles)
            {
                var price = Settings.Prices[vehicle.VehicleType];

                if (vehicle.Balance < Settings.Prices[vehicle.VehicleType] && vehicle.Balance > 0)
                {
                    price = vehicle.Balance + ((Settings.Prices[vehicle.VehicleType] - vehicle.Balance) * Settings.FineCoefficient);
                }
                else if (vehicle.Balance <= 0)
                {
                    price *= Settings.FineCoefficient;
                }

                vehicle.Balance -= price;
                _parking.Balance += price;
                transactions.Add(new TransactionInfo(vehicle.Id, price));
            }
        }

        private void Log(object sender, ElapsedEventArgs e)
        {
            var stringBuilder = new StringBuilder();

            transactions.ForEach(item => stringBuilder.Append(item.ToString() + "\n"));

            _logService.Write(stringBuilder.ToString());
            transactions.Clear();
        }

        public void Dispose()
        {
            _parking.Vehicles.Clear();
            _parking.Balance = 0;
            _logTimer.Stop();
            _withdrawTimer.Stop();
            File.Delete(Settings.LogPath);
        }
    }
}
